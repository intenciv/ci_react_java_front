FROM node:10.19.0-alpine3.9 AS builder
WORKDIR /app
COPY package*.json ./
RUN npm i
COPY . .
#RUN sh replace_url.sh

FROM alpine:3.9
WORKDIR /app
RUN apk update --no-cache && apk add --no-cache npm curl 
COPY --from=builder /app ./
EXPOSE 3000
HEALTHCHECK --interval=20s --timeout=3s \
  CMD curl -f http://localhost:3000 || exit 1
CMD [ "npm", "start" ]
